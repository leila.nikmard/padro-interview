import React from "react";
import history from "./utils/helpers/history";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./Pages/Home";
import Edit from "./Pages/Edit";

const RouterComponent = ({children}) => {
  return (
    <Router history={history}>
      {children}
      <Switch>
        <Route path="/" exact component={Home}></Route>
        <Route path="/edit/:id" exact component={Edit}></Route>
      </Switch>
    </Router>
  );
};

export default RouterComponent;
