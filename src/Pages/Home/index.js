import React, {useState} from "react";
import { useHistory } from "react-router-dom";
import Form from "../../components/ToDoForm"
import Tasks from "../../components/Tasks";
import {useToDoContext} from "../../Providers/ToDoProvider"

const Home = () => {
  const history = useHistory()
  const [todoItem , setTodoItem] = useState({title:"",description:"",status:"ToDo" , id:0});
  const {addTodo , todoList} = useToDoContext();

  const handleChange = (e) => {
    setTodoItem({...todoItem , [e.target.name]:e.target.value, id:todoList.length + 1, status:"ToDo"})
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    addTodo(todoItem);
    setTodoItem({title:"",description:""})
  }

  const handleEdit = (id) => {
    history.push(`/edit/${id}`)
  }  

  const handleSelect = () => {
    
  }

  return (
     <div className="container-fluid">
      <Form  handleSelect={handleSelect} handleChange={handleChange} handleSubmit={handleSubmit} data={todoItem}/>
      <Tasks handleEdit={handleEdit}/>
     </div>
  );
};

export default Home;
