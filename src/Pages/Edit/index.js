import React, { useState } from "react";
import { Redirect, useHistory, useParams } from "react-router-dom";
import Form from "../../components/ToDoForm";
import {useToDoContext} from "../../Providers/ToDoProvider"

const Edit = () => {
    const params = useParams();
    const history = useHistory();
    const {todoList,editTodo} = useToDoContext();
    const id = params?.id
    const data = todoList.find(it => it.id == id);
    const [todoItem , setTodoItem] = useState({...data});

    const handleChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setTodoItem({...todoItem , [name] : value})
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        editTodo({...todoItem});
        setTodoItem({title:"",description:""});
        history.push("/")
    }

    const handleSelect = (e) =>{
        const value = e.target.value;
        setTodoItem({...todoItem , status: value})
    }

    if(!todoItem.hasOwnProperty("id")){
        return <Redirect to="/"/>
    }

    return (
        <div className="container-fluid">
            <Form editable handleSelect ={handleSelect} handleChange={handleChange} handleSubmit={handleSubmit} data={todoItem}/>
            
        </div>
    )

}

export default Edit;