import {useState , createContext, useContext} from "react";

const ToDoContext = createContext();
const initialToDoListState = [];

const ToDoProvider = ({children}) => {
    const [todoList , setTodoList] = useState(initialToDoListState);
    const addTodo = (newTodoItem) => {
        setTodoList([...todoList , newTodoItem])
    }

    const editTodo = (todoItem) => {
        const data = todoList.filter(it => it.id != todoItem.id);
        setTodoList([...data, todoItem])
    }
    const contextValue = {
        todoList,
        addTodo,
        editTodo
    }

    return (<ToDoContext.Provider value={contextValue}>{children}</ToDoContext.Provider>)

}

export const useToDoContext = () => useContext(ToDoContext)

export default ToDoProvider;
