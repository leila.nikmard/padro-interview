import styled from "styled-components";

export const StyleWrapper = styled.div`
  header {
    padding: 5px 50px;
    background-color:#1775b9;
    .title {
      font-weight: 700;
      font-size: 22px;
      color:white;
      @media(max-width:768px){
        font-size: 18px;
      }
    }
  }
`;
