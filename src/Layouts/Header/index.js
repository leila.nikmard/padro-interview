import React, { useEffect } from "react";
import { useLocation,matchPath } from "react-router-dom";

import { StyleWrapper } from "./header.style.js";

const Header = () => {
  const location = useLocation();
  return (
    <StyleWrapper>
      <header>
        <p className="title">{`Task Management > ${location.pathname === "/" ? "Home" : "Edit"}`}</p>
      </header>
    </StyleWrapper>
  );
};

export default Header;
