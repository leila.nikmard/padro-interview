import React from "react";
import { useHistory } from "react-router-dom";
import StatusSelector from "../StatusSelector";
import {plusIcon,editWhiteIcon} from "../../images"

import { StyleWrapper } from "./todoForm.style";

const Form = ({handleChange , handleSubmit , data,handleSelect,editable}) => {
  const history = useHistory();
  const handleCancel = () => {
    history.push("/")
  };

  return (
    <StyleWrapper>
      <form onSubmit={handleSubmit}>
        <div className="form-container">
        <div>
            <h2 className="form-title">{!editable ? "Add a new Task" : "Edit Task"}</h2>
          </div>
            <div className="g">
              <input 
                type="text" 
                name="title" 
                value={data.title}
                placeholder="Title" 
                style={{borderBottomColor: data.title !== "" && "#1775b9"}}
                onChange={e => handleChange(e)}
              />
            </div>
            <div className="g">
              <textarea 
                type="text" 
                name="description" 
                value={data.description}
                placeholder="Description"
                rows={editable ? 25 : 8}
                style={{borderBottomColor: data.description !== "" && "#1775b9"}}
                onChange={e => handleChange(e)}
              />
            </div>
            {editable && <div>
              <StatusSelector status={data.status} handleSelect={handleSelect}/>
            </div>}
            <div className="container-btn">
              <button 
                type="submit"
                disabled={data?.title == "" || data?.description == ""} 
                className={`add-edit-btn ${!editable ? "add-btn" : ""}`}>
                  <img src={editable ? editWhiteIcon : plusIcon} width="12px" />
                  {editable ? "Edit" : "Add"}
                </button>
              {editable && <button className="cancel-btn" onClick={handleCancel}>Cancel</button>}
            </div>
        </div>
      </form>
    </StyleWrapper>
  );
};

export default Form;
