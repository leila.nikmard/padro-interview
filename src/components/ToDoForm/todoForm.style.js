import styled from "styled-components";

export const StyleWrapper = styled.div`
    .form-container{
        padding:0px 40px;

        @media(max-width:768px){
            padding:5px 40px;
        }

        .form-title{
            padding: 10px 0;
            @media(max-width:768px){
                font-size:18px;
                padding:5px 0;
            }
        }
        
        input,
        textarea{
            width:100%;
            box-sizing: border-box;
            border:none;
            outline:none;
            border-bottom: 1px solid gray;
            background-color:#e9eaeb;
            border-radius: 4px 4px 0 0;
            padding :12px 16px;
            font-size:18px;
            margin : 0 auto 15px auto;
            @media(max-width:768px){
                padding :8px 12px;
                font-size:16px;
            }
        }

        textarea{
            line-height : 18px;
        }
    
        .container-btn{
            display:flex;
            justify-content:space-between;
            .cancel-btn,
            .add-edit-btn,
            .add-btn{
                border:none;
                background-color:white;
                padding: 18px 0;
                border:1px solid lightgray;
                border-radius:5px;
                font-size:16px;
                width:48%;
                @media(max-width:768px){
                    padding: 12px 0;
                    font-size:14px;
                }
            }
        
            .add-edit-btn{
                color:white;
                background-color:#1775b9
            }

            .add-btn{
                width: 100%
            }

            img{
                margin-right:12px;
            }
        }
    }
  
`;
