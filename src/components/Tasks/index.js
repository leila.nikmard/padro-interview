import React from "react";
import Task from "../Task";
import {useToDoContext} from "../../Providers/ToDoProvider";

import {StyleWrapper} from "./tasks.style"

const Tasks = ({handleEdit}) => {
   const {todoList} = useToDoContext();

    return(
        <StyleWrapper>
            <div className="container">
                <p className="title">Tasks</p>
                <div className="container-tasks">
                    {todoList.length ? todoList.map((item,index) => (<Task key={index} data={item} handleEdit={handleEdit}/>)) 
                    : 
                    <p className="no-task">You have nothing to do.<br/> Go get some sleep.</p>
                    }
                </div>
            </div>
        </StyleWrapper>
    )
}

export default Tasks