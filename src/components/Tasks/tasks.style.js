import styled from "styled-components";

export const StyleWrapper = styled.div`
    .container{
        width:100%;
        border-radius: 15px 15px 0 0;
        background-color:#1775b9;
        position : relative;

        .title{
            padding : 20px 30px 10px 30px;
            color:white;
            font-weight: 600;
            font-size:20px;
            @media(max-width:768px){
                font-size:18px;
            }
        }

        .container-tasks{
            height:260px;
            border-radius: 15px 15px 0 0;
            background-color:#a2ceed;
            position:relative;
            padding: 5% 0 4% 6%;
            overflow-y: auto;

            @media(max-width: 638px){
                padding: 5% 0 3% 5%;
            }

            @media(max-width: 536px){
                padding: 5% 0 2% 4%;
            }

            @media(max-width: 470px){
                padding:20px 0 ;
                display:flex;
                justify-content: center;
                align-items:center;
                flex-wrap:wrap;
            }

            .no-task{
                width:100%;
                text-align:center;
                font-size:20px;
                font-weight: 600;
                position:absolute;
                top:50%;
                left:50%;
                transform:translate(-50%,-50%);
            }
           
        }
    }
`;
