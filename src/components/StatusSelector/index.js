import React from "react";
import { useParams } from "react-router-dom";
import { useToDoContext } from "../../Providers/ToDoProvider";

import {StyleWrapper} from "./statusSelector.style"

const StatusSelector = ({status,handleSelect}) => {
  const {todoList} = useToDoContext();
  const params = useParams();
  const data = todoList.find(it => it.id == params.id)?.status;

  const statusData = {
    "InProgress": ["InQA","InProgress", "Blocked"],
    "InQA": ["Done","InQA"],
    "Done" : ["Deployed","Done"],
    "ToDo": ["ToDo","InProgress"],
    "Blocked": ["Blocked","ToDo"]
  }

  return (
    <StyleWrapper>
      <select style={{borderBottomColor: statusData && "#1775b9"}} value={status} onChange={handleSelect}>
        {statusData[data] && statusData[data].map((it,index) => <option value={it} key={index} >{it}</option>)}
      </select>
    </StyleWrapper>
  );
};

export default StatusSelector;
