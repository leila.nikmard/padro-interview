import styled from "styled-components";

export const StyleWrapper = styled.div`
    select{
        border: none;
        outline:none;
        width: 100%;
        border-bottom:1px solid gray;
        background-color:#e9eaeb;
        border-radius: 5px 5px 0 0 ;
        padding: 10px;
        margin-bottom:15px;
    }
  
`;
