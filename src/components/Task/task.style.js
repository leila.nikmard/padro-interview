import styled from "styled-components";

export const StyleWrapper = styled.div`
   .task-container{
        width: 40%;
        float:left;
        background-color:white;
        border-radius:5px;
        padding: 15px;
        box-shadow : 3px 3px 8px gray;
        margin: 0 5% 5% 0%;
        @media(max-width: 638px){
            margin: 0 3% 3% 0%;
        }
        @media(max-width: 536px){
            margin: 0 2% 2% 0%;
        }

        @media(max-width: 470px){
            width:250px;
            margin-bottom:20px;
        }

        .task-title{
            word-wrap:break-word;
            font-weight : 700;
            font-size:20px;
            @media(max-width: 768px){
                font-size:16px;
            }
            
        }

        .task-descroption{
            word-wrap:break-word;
            overflow:visible;
            padding: 20px 0;
            font-size:16px;
            min-height:122px;
            @media(max-width: 768px){
                font-size:14px;
                min-height:82px;
            }
        }

        .task-status{
            display:flex;
            justify-content:space-between;
            align-items:center;
            .status{
                border-radius: 10px;
                background-color: #1775b9;
                padding: 5px 30px;
                color:white;
                @media(max-width: 768px){
                    font-size:12px;
                }
            }

            img{
                cursor:pointer;
            }
        }

   }
`;
