import React from "react";
import { editBlackIcon } from "../../images";


import {StyleWrapper} from "./task.style"

const Task = ({data , handleEdit}) => {
    return(
        <StyleWrapper>
            <div className="task-container">
               <div className="task-title">{data?.title}</div>
               <div className="task-descroption">{data?.description}</div>
               <div className="task-status">
                <span className="status">{data?.status}</span>
                    <img src={editBlackIcon} onClick={() => handleEdit(data?.id)} width="25px" height="25px" />
               </div>
            </div>
        </StyleWrapper>
    )
}

export default Task