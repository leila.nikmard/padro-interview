import plusIcon from "./assets/images/plus.png";
import editBlackIcon from "./assets/images/edit-black.png";
import editWhiteIcon from "./assets/images/edit-white.png";

export {
    plusIcon,
    editBlackIcon,
    editWhiteIcon
}