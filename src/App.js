import React from "react";
import Header from "./Layouts/Header";
import RouterComponent from "./routes";
import ToDoProvider from "./Providers/ToDoProvider";

import { StyleWrapper } from "./App.style";

function App() {
  return (
    <StyleWrapper>
      <ToDoProvider>
        <RouterComponent>
          <Header/>
        </RouterComponent>
      </ToDoProvider>
    </StyleWrapper>
  );
}

export default App;
