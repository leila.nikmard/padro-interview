import styled from "styled-components";

export const StyleWrapper = styled.div`
.container-fluid{
  width:770px;
  margin : 0 auto;

  @media(max-width:768px){
    width:100%;
  }
}
`;
